﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ThreadsInWPF.Code
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region First Thread
        private void BtnPutIn1_Click(object sender, RoutedEventArgs e)
        {
            if (lbFruits.SelectedItem != null)
            {
                var fruit = (lbFruits.SelectedItem as ListBoxItem).Content;
                lbBlender1.Items.Add(new ListBoxItem { Content = fruit });
            }
            btnClean1.IsEnabled = true;
        }

        private void BtnBlend1_Click(object sender, RoutedEventArgs e)
        {
            Thread thread1 = new Thread(Blend1);
            thread1.Start();
        }

        private void btnClean1_Click(object sender, RoutedEventArgs e)
        {
            Thread thread3 = new Thread(Clean1);
            thread3.Start();
        }

        private void Blend1()
        {
            ButtonDisabling1();

            // BLENDING TIME, BOYS!
            int blendTime = 100;
            for (int i = 0; i <= blendTime; i++)
            {
                lblStatus1.Dispatcher.Invoke(new Action(() => lblStatus1.Content = $"Blending {i}"), DispatcherPriority.Normal, null);
                pb1.Dispatcher.Invoke(new Action(() => pb1.Value++), DispatcherPriority.Normal, null);

                Thread.Sleep(25);
            }
            Thread.Sleep(250);
            pb1.Dispatcher.Invoke(new Action(() => pb1.Value = 0), DispatcherPriority.Normal, null);

            ImageHandling1();

            ButtonEnabling1();

            Shipping1Stuff();
        }

        private void Clean1()
        {
            // CLEANS THE BLENDER AND UPDATES THE STATUS
            lbBlender1.Dispatcher.Invoke(new Action(() => lbBlender1.Items.Clear()), DispatcherPriority.Normal, null);

            lblStatus1.Dispatcher.Invoke(new Action(() => lblStatus1.Content = "Cleaned"), DispatcherPriority.Normal, null);

            btnClean1.Dispatcher.Invoke(new Action(() => btnClean1.IsEnabled = false), DispatcherPriority.Normal, null);
        }

        private void Shipping1Stuff()
        {
            // SECTION FOR SHIPPING LABEL
            Thread.Sleep(1150);
            Shipping1.Dispatcher.Invoke(new Action(() => Shipping1.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            dot13.Dispatcher.Invoke(new Action(() => dot13.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            dot12.Dispatcher.Invoke(new Action(() => dot12.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            dot11.Dispatcher.Invoke(new Action(() => dot11.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            Shipping1.Dispatcher.Invoke(new Action(() => Shipping1.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);
            dot13.Dispatcher.Invoke(new Action(() => dot13.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);
            dot12.Dispatcher.Invoke(new Action(() => dot12.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);
            dot11.Dispatcher.Invoke(new Action(() => dot11.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);

            // REMOVES THE IMAGE AT THE SAME TIME AS THE SHIPPING LABEL
            img1.Dispatcher.Invoke(new Action(() => img1.Source = null), DispatcherPriority.Normal, null);
        }

        private void ButtonDisabling1()
        {
            // DISABLES BUTTONS WHILE BLENDING, DON'T HURT YOURSELF.
            btnBlend1.Dispatcher.Invoke(new Action(() => btnBlend1.IsEnabled = false), DispatcherPriority.Normal, null);
            btnClean1.Dispatcher.Invoke(new Action(() => btnClean1.IsEnabled = false), DispatcherPriority.Normal, null);
            btnPutIn1.Dispatcher.Invoke(new Action(() => btnPutIn1.IsEnabled = false), DispatcherPriority.Normal, null);
        }

        private void ButtonEnabling1()
        {
            // ENABLES BUTTONS AFTER RUNNING
            btnClean1.Dispatcher.Invoke(new Action(() => btnClean1.IsEnabled = true), DispatcherPriority.Normal, null);
            lblStatus1.Dispatcher.Invoke(new Action(() => lblStatus1.Content = "Juice Ready"), DispatcherPriority.Normal, null);
            btnBlend1.Dispatcher.Invoke(new Action(() => btnBlend1.IsEnabled = true), DispatcherPriority.Normal, null);
            btnPutIn1.Dispatcher.Invoke(new Action(() => btnPutIn1.IsEnabled = true), DispatcherPriority.Normal, null);
        }

        private void ImageHandling1()
        {
            // MAKES A RANDOM NUMBER TO USE FOR PICKING A RANDOM IMAGE TO SHOW IN THE WINDOW
            Random random = new Random();

            string i = random.Next(1, 14).ToString();

            img1.Dispatcher.Invoke(new Action(() => img1.Source = new BitmapImage(new Uri($"C:/Users/mikke/Desktop/ThreadsInWPF.Code/ThreadsInWPF.Code/Smoothies/{i}.png"))), DispatcherPriority.Normal, null);
        }
        #endregion



        #region Second Thread
        private void BtnPutIn2_Click(object sender, RoutedEventArgs e)
        {
            if (lbFruits.SelectedItem != null)
            {
                var fruit = (lbFruits.SelectedItem as ListBoxItem).Content;
                lbBlender2.Items.Add(new ListBoxItem { Content = fruit });
            }
            btnClean2.IsEnabled = true;
        }

        private void BtnBlend2_Click(object sender, RoutedEventArgs e)
        {
            Thread thread2 = new Thread(Blend2);
            thread2.Start();
        }

        private void btnClean2_Click(object sender, RoutedEventArgs e)
        {
            Thread thread4 = new Thread(Clean2);
            thread4.Start();
        }

        private void Blend2()
        {
            ButtonDisabling2();

            // BLENDING TIME, BOYS!
            int blendTime = 100;
            for (int i = 0; i <= blendTime; i++)
            {
                lblStatus2.Dispatcher.Invoke(new Action(() => lblStatus2.Content = $"Blending {i}"), DispatcherPriority.Normal, null);
                pb2.Dispatcher.Invoke(new Action(() => pb2.Value++), DispatcherPriority.Normal, null);

                Thread.Sleep(25);
            }
            Thread.Sleep(250);
            pb2.Dispatcher.Invoke(new Action(() => pb2.Value = 0), DispatcherPriority.Normal, null);

            ImageHandling2();

            ButtonEnabling2();

            Shipping2Stuff();
        }

        private void Clean2()
        {
            lbBlender2.Dispatcher.Invoke(new Action(() => lbBlender2.Items.Clear()), DispatcherPriority.Normal, null);

            lblStatus2.Dispatcher.Invoke(new Action(() => lblStatus2.Content = "Cleaned"), DispatcherPriority.Normal, null);

            btnClean2.Dispatcher.Invoke(new Action(() => btnClean2.IsEnabled = false), DispatcherPriority.Normal, null);
        }

        private void Shipping2Stuff()
        {
            // SECTION FOR SHIPPING LABEL
            Thread.Sleep(1150);
            Shipping2.Dispatcher.Invoke(new Action(() => Shipping2.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            dot23.Dispatcher.Invoke(new Action(() => dot23.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            dot22.Dispatcher.Invoke(new Action(() => dot22.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            dot21.Dispatcher.Invoke(new Action(() => dot21.Visibility = Visibility.Visible), DispatcherPriority.Normal, null);
            Thread.Sleep(1150);
            Shipping2.Dispatcher.Invoke(new Action(() => Shipping2.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);
            dot23.Dispatcher.Invoke(new Action(() => dot23.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);
            dot22.Dispatcher.Invoke(new Action(() => dot22.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);
            dot21.Dispatcher.Invoke(new Action(() => dot21.Visibility = Visibility.Hidden), DispatcherPriority.Normal, null);

            // REMOVES THE IMAGE AT THE SAME TIME AS THE SHIPPING LABEL
            img2.Dispatcher.Invoke(new Action(() => img2.Source = null), DispatcherPriority.Normal, null);
        }

        private void ButtonDisabling2()
        {
            // DISABLES BUTTONS WHILE BLENDING, DON'T HURT YOURSELF.
            btnBlend2.Dispatcher.Invoke(new Action(() => btnBlend2.IsEnabled = false), DispatcherPriority.Normal, null);
            btnClean2.Dispatcher.Invoke(new Action(() => btnClean2.IsEnabled = false), DispatcherPriority.Normal, null);
            btnPutIn2.Dispatcher.Invoke(new Action(() => btnPutIn2.IsEnabled = false), DispatcherPriority.Normal, null);
        }

        private void ButtonEnabling2()
        {
            // ENABLES BUTTONS AFTER RUNNING
            btnClean2.Dispatcher.Invoke(new Action(() => btnClean2.IsEnabled = true), DispatcherPriority.Normal, null);
            lblStatus2.Dispatcher.Invoke(new Action(() => lblStatus2.Content = "Juice Ready"), DispatcherPriority.Normal, null);
            btnBlend2.Dispatcher.Invoke(new Action(() => btnBlend2.IsEnabled = true), DispatcherPriority.Normal, null);
            btnPutIn2.Dispatcher.Invoke(new Action(() => btnPutIn2.IsEnabled = true), DispatcherPriority.Normal, null);
        }

        private void ImageHandling2()
        {
            // MAKES A RANDOM NUMBER TO USE FOR PICKING A RANDOM IMAGE TO SHOW IN THE WINDOW
            Random random = new Random();

            string i = random.Next(1, 14).ToString();

            img2.Dispatcher.Invoke(new Action(() => img2.Source = new BitmapImage(new Uri($"C:/Users/mikke/Desktop/ThreadsInWPF.Code/ThreadsInWPF.Code/Smoothies/{i}.png"))), DispatcherPriority.Normal, null);
        }
        #endregion
    }
}
