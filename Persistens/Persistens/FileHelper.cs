﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Persistens
{
    public class FileHelper
    {
        public static void AddEntry(string newEntry)
        {
            StreamReader reader = new StreamReader("entrylog.txt");
            string file = reader.ReadToEnd();
            reader.Close();

            // If the array is 1 entity long, and the first line is blank.. THEN OVERWRITE THE DOCUMENT
            if (file == "\r\n")
            {
                StreamWriter writer = new StreamWriter("entrylog.txt");
                writer.WriteLine(newEntry);
                writer.Close();
            }
            else
            {
                StreamWriter writer = new StreamWriter("entrylog.txt", true);
                writer.WriteLine(newEntry);
                writer.Close();
            }
        }

        public static void RemoveEntry(int lineNumber)
        {
            // Moves content of the .txt file into an array.
            string[] textContent = File.ReadAllLines("entrylog.txt");

            // Creates a new array with same length of the former -1.
            string[] newTextContent = new string[textContent.Length - 1];

            bool lineSkipped = false;

            // Goes through all the entries in the array.
            for(int i = 1; i <= textContent.Length; i++)
            {
                // if the current number on the counter does not match
                // lineNumber, the old content moves over to new array.
                if(i != lineNumber)
                {
                    if (lineSkipped)
                    {
                        newTextContent[i - 2] = textContent[i - 1];
                    }
                    else
                    {
                        newTextContent[i - 1] = textContent[i - 1];
                    }
                }
                else
                {
                    lineSkipped = true;
                    continue;
                }
            }
            File.WriteAllLines("entrylog.txt", newTextContent);
        }

        public static void UpdateEntry(int lineNumber, string newEntry)
        {
            // Moves content of the .txt file into an array.
            string[] textContent = File.ReadAllLines("entrylog.txt");

            // New entry gets put into box fitting lineNumber.
            textContent[lineNumber - 1] = newEntry;
            StreamWriter writer = new StreamWriter("entrylog.txt");

            // Writes the new content into entrylog.txt
            foreach(string line in textContent)
            {
                writer.WriteLine(line);
            }
            writer.Close();
        }

        public static string DisplayEntry(int lineNumber)
        {
            string[] textContent = File.ReadAllLines("entrylog.txt");
            Console.WriteLine(textContent[lineNumber - 1]);
            return textContent[lineNumber - 1];
        }

        public static void DisplayEntries()
        {
            StreamReader reader = new StreamReader("entrylog.txt");
            while (reader.EndOfStream == false)
            {
                string line = reader.ReadLine();
                Console.WriteLine("\t{0}", line);
            }
            reader.Close();

            Console.WriteLine("\n");

        }

        public static void ClearFile()
        {
            StreamWriter sw = new StreamWriter("entrylog.txt");
            sw.WriteLine("");
            sw.Close();
        }

        public static bool FileEmpty()
        {
            StreamReader reader = new StreamReader("entrylog.txt");
            string file = reader.ReadToEnd();
            reader.Close();

            if (new FileInfo("entrylog.txt").Length == 0 || file == "\r\n")
                return true;
            else
                return false;
        }
    }
}