﻿using System;
using System.Dynamic;

namespace Persistens
{
    class Program
    {
        static void Main(string[] args)
        {
            AddMenu();
        }


        public static void AddMenu()
        {
            bool looping = true;

            do
            {
                Console.WriteLine("## MENU ##\n");

                string[] menuItems = { "1) Add entry", "2) Remove entry", "3) Update entry", "4) View specific entry", "5) View list of entries", "6) Exit" };

                for (int i = 0; i < menuItems.Length; i++)
                {
                    Console.WriteLine(menuItems[i]);
                }

                int exitMenu = Convert.ToInt32(Console.ReadLine());

                switch (exitMenu)
                {
                    case 1:
                        Console.WriteLine("What item would you like to add?");
                        FileHelper.AddEntry(Console.ReadLine());
                        break;
                    case 2:
                        Console.WriteLine("What item would you like deleted?");
                        FileHelper.RemoveEntry(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case 3:
                        Console.WriteLine("What item number would you like to edit? and with what?");
                        FileHelper.UpdateEntry(Convert.ToInt32(Console.ReadLine()), Console.ReadLine());
                        break;
                    case 4:
                        Console.WriteLine("What item would you like to view?");
                        FileHelper.DisplayEntry(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case 5:
                        FileHelper.DisplayEntries();
                        break;
                    case 6:
                        looping = false;
                        break;
                }

            } while (looping);
        }
    }
}
