﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Fucking_About
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Types of Sorting Arrays.\nUnsorted Array:");
            int[] arr = { 5, 2, 4, 7, 2, 7, 3, 7, 1, 10, 15, 2, 1, 9, 4 };
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }

            Stopwatch stopwatch1 = new Stopwatch();
            Stopwatch stopwatch2 = new Stopwatch();

            int winsMethod = 0;
            int winsBubble = 0;
            double counter1 = 0;
            double counter2 = 0;

            for (int i = 0; i < 100; i++)
            {
                stopwatch1.Start();
                BubbleSort();
                stopwatch1.Stop();
                counter1 = counter1 + stopwatch1.Elapsed.TotalMilliseconds;

                stopwatch2.Start();
                MethodSort();
                stopwatch2.Stop();
                counter2 = counter2 + stopwatch2.Elapsed.TotalMilliseconds;

                if (stopwatch2.Elapsed.TotalMilliseconds > stopwatch1.Elapsed.TotalMilliseconds)
                {
                    winsBubble = winsBubble + 1;
                }
                else if (stopwatch1.Elapsed.TotalMilliseconds > stopwatch2.Elapsed.TotalMilliseconds)
                {
                    winsMethod = winsMethod + 1;
                }
            }

            double avg1 = counter1 / 100;
            double avg2 = counter2 / 100;

            Console.WriteLine("\n\n\nWins for Bubblesort: {0}/100\nAverage: {1}", winsBubble, avg1 * 1000000);
            Console.WriteLine("\nWins for Methodsort: {0}/100\nAverage: {1}", winsMethod, avg2 * 1000000);

            Console.ReadKey();
        }

        public static void BubbleSort()
        {
            int[] arr = { 5, 2, 4, 7, 2, 7, 3, 7, 1, 10, 15, 2, 1, 9, 4 };
            int swapper = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        swapper = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = swapper;
                    }
                }
            }
            /*
            Console.WriteLine("\n\nBubbleSort:");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
            */
        }

        public static void MethodSort()
        {
            int[] arr = { 5, 2, 4, 7, 2, 7, 3, 7, 1, 10, 15, 2, 1, 9, 4 };
            Array.Sort(arr);

            /*
            Console.WriteLine("\n\nMethodSort:");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
            */
        }
    }
}