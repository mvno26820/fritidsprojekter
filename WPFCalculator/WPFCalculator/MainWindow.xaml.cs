﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Mathematics maths;
        public MainWindow()
        {
            InitializeComponent();

            maths = new Mathematics();
        }

        private void Addition_Click(object sender, RoutedEventArgs e)
        {
            tbLine3.Text = maths.Add(Convert.ToInt32(tbLine1.Text), Convert.ToInt32(tbLine2.Text)).ToString();
        }

        private void Subtraction_Click(object sender, RoutedEventArgs e)
        {
            tbLine3.Text = maths.Subtract(Convert.ToInt32(tbLine1.Text), Convert.ToInt32(tbLine2.Text)).ToString();
        }

        private void Multiplication_Click(object sender, RoutedEventArgs e)
        {
            tbLine3.Text = maths.Multiply(Convert.ToInt32(tbLine1.Text), Convert.ToInt32(tbLine2.Text)).ToString();
        }

        private void Division_Click(object sender, RoutedEventArgs e)
        {
            tbLine3.Text = maths.Divide(Convert.ToInt32(tbLine1.Text), Convert.ToInt32(tbLine2.Text)).ToString();
        }
    }
}
