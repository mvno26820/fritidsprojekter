﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GRMyTryAtGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private OpretHylderPage p1 = new OpretHylderPage();
        private SettingsPage p2 = new SettingsPage();

        private void btnpage1(object sender, RoutedEventArgs e)
        {
            Main.Content = p1.Content;
        }

        private void btnPage2(object sender, RoutedEventArgs e)
        {
            Main.Content = p2.Content;
        }
    }
}
