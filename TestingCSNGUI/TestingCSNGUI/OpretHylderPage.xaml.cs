﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestingCSNGUI;

namespace GRMyTryAtGUI
{
    /// <summary>
    /// Interaction logic for OpretHylderPage.xaml
    /// </summary>
    public partial class OpretHylderPage : Page
    {
        public OpretHylderPage()
        {
            InitializeComponent();

            List<TestingTable> users = new List<TestingTable>();
            users.Add(new TestingTable() { Række = 14, Fag = 25, Inkluder = false });
            users.Add(new TestingTable() { Række = 24, Fag = 35, Inkluder = false });
            users.Add(new TestingTable() { Række = 34, Fag = 45, Inkluder = false });
            users.Add(new TestingTable() { Række = 34, Fag = 45, Inkluder = false });

            DataGridSimple.ItemsSource = users;
        }
    }
}
