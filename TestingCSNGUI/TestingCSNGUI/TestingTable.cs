﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingCSNGUI
{
    class TestingTable
    {
        public int Række { get; set; }
        public int Fag { get; set; }
        public bool Inkluder { get; set; }
    }
}
