import java.util.Scanner;

public class Roman{
	public static void main(String[]args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please input your desired integer: ");
		System.out.println("When in Rome...: "+integerToRomanNumeral(scanner.nextInt()));
	}
	
	public static String integerToRomanNumeral(int input){
		if(input < 1 || input > 3999){
			return "you know what? no, fuck that, if you're gonna be like that, get out of Rome.";
		}
		String s = "";
		while(input >= 1000){
			s = s + "M";
			input = input - 1000;
		}
		while(input >= 900){
			s = s + "CM";
			input = input - 900;
		}
		while(input >= 500){
			s = s + "D";
			input = input - 500;
		}
		while(input >= 400){
			s = s + "CD";
			input = input - 400;
		}
		while(input >= 100){
			s = s + "C";
			input = input - 100;
		}
		while(input >= 90){
			s = s + "XC";
			input = input - 90;
		}
		while(input >= 50){
			s = s + "L";
			input = input - 50;
		}
		while(input >= 40){
			s = s + "XL";
			input = input - 40;
		}
		while(input >= 10){
			s = s + "X";
			input = input - 10;
		}
		while(input >= 9){
			s = s + "IX";
			input = input - 9;
		}
		while(input >= 5){
			s = s + "V";
			input = input - 5;
		}
		while(input >= 4){
			s = s + "IV";
			input = input - 4;
		}
		while(input >= 1){
			s = s + "I";
			input = input - 1;
		}
		return s;
	}
}