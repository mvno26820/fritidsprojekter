import java.util.Scanner;

public class Calories{
	/*
	* Main method which guides the user to the correct calculating area.
	*/
	public static void main(String[]args){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to the Harris Benedict Equation Calculater.");
		System.out.println("First of all, I need to know how active you are.");
		System.out.println("Of the 5 options below, what fits you the most?");
		System.out.println("1. little or no exercise.");
		System.out.println("2. light exercise 1-3 days/week.");
		System.out.println("3. moderate exercise 3-5 days/week.");
		System.out.println("4. hard exercise 6-7 days/week.");
		System.out.println("5. very hard exercise & physical job or 2x training.");
		int active = scanner.nextInt();
		if(active < 1 || active >5){
			System.out.println("I see what you're trying to do here, fuck off.");
		}
		switch(active){
			case 1:
				System.out.print("Please input weight, height and age respectively: ");
				System.out.println("You need to take in "+bmr(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble())*1.2+" calories/day.");
				break;
			case 2:
				System.out.print("Please input weight, height and age respectively: ");
				System.out.println("You need to take in "+bmr(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble())*1.375+" calories/day.");
				break;
			case 3:
				System.out.print("Please input weight, height and age respectively: ");
				System.out.println("You need to take in "+bmr(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble())*1.55+" calories/day.");
				break;
			case 4:
				System.out.print("Please input weight, height and age respectively: ");
				System.out.println("You need to take in "+bmr(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble())*1.725+" calories/day.");
				break;
			case 5:
				System.out.print("Please input weight, height and age respectively: ");
				System.out.println("You need to take in "+bmr(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble())*1.9+" calories/day.");
				break;
			default:
				System.out.println("It would appear that something went wrong...");
		}
	}
	
	/*
	* Method that takes weight as input and returns a new 'weight' that functions
	* as the required BMR.
	*/
	public static double bmr(double weight, double height, double age){
		double kcal=0.0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Are you male or female?");
		String s = scanner.nextLine();
		switch(s){
			case "male":
				kcal=(10*weight)+(6.25*height)-(5*age)+5.0;
				break;
			case "female":
				kcal=(10*weight)+(6.25*height)-(5*age)-161.0;
				break;
			default:
				System.out.println("sorry, try again, all small letters next time.");
				break;
		}
		return kcal;
	}
}