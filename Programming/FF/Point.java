import java.util.Scanner;

public class Point{
	public static void main(String[]args){
		Scanner scanner=new Scanner(System.in);
		System.out.println("  (^-^)  ");
		System.out.println("__  |  __");
		System.out.println("  \\[ ]/  ");
		System.out.println("   [ ]   ");
		System.out.println("  /   \\  ");
		System.out.println(" |     | ");
		System.out.println("Where on the figure do you want to point:");
		System.out.println("head, arms, belly or perhaps on the legs?");
		String pointing = scanner.nextLine();
		switch(pointing){
			case ("head"):
				System.out.println("   --> (^-^)  ");
				System.out.println("     __  |  __");
				System.out.println("       \\[ ]/  ");
				System.out.println("        [ ]   ");
				System.out.println("       /   \\  ");
				System.out.println("      |     | ");
				break;
			
			case ("arms"):
				System.out.println("       (^-^)  ");
				System.out.println(" --> __  |  __");
				System.out.println("       \\[ ]/  ");
				System.out.println("        [ ]   ");
				System.out.println("       /   \\  ");
				System.out.println("      |     | ");
				break;
			
			case ("belly"):
				System.out.println("       (^-^)  ");
				System.out.println("     __  |  __");
				System.out.println("       \\[ ]/  ");
				System.out.println("   -->  [ ]   ");
				System.out.println("       /   \\  ");
				System.out.println("      |     | ");
				break;
			
			case ("legs"):
				System.out.println("       (^-^)  ");
				System.out.println("     __  |  __");
				System.out.println("       \\[ ]/  ");
				System.out.println("        [ ]   ");
				System.out.println("       /   \\  ");
				System.out.println(" -->  |     | ");
				break;
			
			default:
				System.out.println("Try again, buddy.");
				break;
		}	
	}
}