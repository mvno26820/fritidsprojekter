import java.util.Scanner;

/*
* A magic 8Ball that returns very specific answers, as opposed to "yes", "no", "maybe" etc.
*/
public class Wisdom{
	public static void main(String[]args){
		// Draws the 8Ball and asks the user what sort of questions will be asked.
		System.out.println("You may ask the magic 8Ball any question you wish");
		System.out.println("   ~         ~         ___           ~           ");
		System.out.println("         ~           /     \\   ~           ~    ");
		System.out.println("     ~          ~   |   8   |     ~    ~         ");
		System.out.println("         ~           \\     /             ~      ");
		System.out.println("    ~        ~         ¨¨¨      ~             ~  ");
		Scanner scanner = new Scanner(System.in);
		System.out.println("First, what sort of question would you like to ask?");
		System.out.println("[1] yes/no/maybe/etc. questions");
		System.out.println("[2] life advice");
		System.out.println("[3] opinions");
		int decision = scanner.nextInt();
		// Switch-statement that will essentially have other switch-statements inside them, by using other methods.
		switch(decision){
			case 1:
				yesNo();
				break;
			
			case 2:
				lifeAdvice();
				break;
			
			case 3:
				opinion();
				break;
			
			default:
				System.out.println("Please enter valid value.");
				break;
		}
	}
	
	
	/*
	* A method for case 1.
	*/
	public static void yesNo(){
		System.out.println("[Your question?]");
		Scanner scanner = new Scanner(System.in);
		String yesNoQuestion = scanner.nextLine();
		// Answers to question.
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("No.");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("[Yes.]");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("[Let me think about it.]");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("[Forget about it.]");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("[For sure, 100%]");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("[0%, nope, nah.]");	
		else
			System.out.println("[inconclusive.]");
	}
	
	
	/*
	* A method for case 2.
	*/
	public static void lifeAdvice(){
	System.out.println("[A fortune cookie falls before you, will you open it?]");
		Scanner scanner = new Scanner(System.in);
		String advice = scanner.nextLine();
		switch(advice){
			case ("yes"):
				if(randomInteger(0.0, 10.0)<1.0)
					System.out.println("[Focus on worrying about one thing in life at a time.]");
				else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.0)
					System.out.println("[Advice, when most needed, is least heeded.]");
				else if(2.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.0)
					System.out.println("[Everything in moderation, including moderation.]");
				else if(3.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<4.0)
					System.out.println("[Don't count the days, make the days count.]");
				else if(4.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
					System.out.println("[It always seems impossible until it's done.]");
				else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<6.0)
					System.out.println("[Your life is your message, make it a good one.]");
				else if(6.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.0)
					System.out.println("[You must be the change you wish to see in the world.]");
				else if(7.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<8.0)
					System.out.println("[Life is actually really simple, we choose to make it complicated.]");
				else if(8.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<9.0)
					System.out.println("[You may not be able to change the world, but a rock in water creates many ripples.]");
				else if(9.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
					System.out.println("[Say 'yes' to more things in life, at least more than 'no'.]");
				else if(10.0==randomInteger(0.0, 10.0))
					System.out.println("[Life carries no meaning, don't look for it, make it.]");
				else
					System.out.println("[inconclusive.]");
				break;
			
			case ("no"):
				System.out.println("[Then why the hell did you even choose [2]?]");
				break;
		}
	}
	
	
	/*
	* A method for case 3.
	*/
	public static void opinion(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("[What would you like my opinion on?]");
		System.out.println("[1] Looks");
		System.out.println("[2] Movies/TV Shows");
		System.out.println("[3] People");
		int decision = scanner.nextInt();
		switch(decision){
			case 1:
				looks();
				break;
			
			case 2:
				moviesAndShows();
				break;
			
			case 3:
				people();
				break;
			
			default:
				System.out.println("[Please try again later, and use a valid input next time.]");
				break;
		}
	}
	
	
	public static void looks(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("[Would you like to be judged?]");
		String yesOrNo = scanner.nextLine();
		switch(yesOrNo){
			case("yes"):
				looksOutput();
				break;
				
			case("no"):
				System.out.println("I see that your heart is weak, goodbye.");
				break;
				
			default:
				System.out.println("Now why did you have to crash the program? why?");
				break;
		}

	}
	
	
	public static void moviesAndShows(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("[Would you honestly like my opinion on that?]");
		String yesOrNo = scanner.nextLine();
		switch(yesOrNo){
			case("yes"):
				moviesAndShowsOutput();
				break;
				
			case("no"):
				System.out.println("... cya later.");
				break;
				
			default:
				System.out.println("Now why did you have to crash the program? why?");
				break;
		}
	}
	
	
	public static void people(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("[Would you honestly like my opinion on a person?]");
		String yesOrNo = scanner.nextLine();
		switch(yesOrNo){
			case("yes"):
				peopleOutput();
				break;
				
			case("no"):
				System.out.println("... alright, I'm out.");
				break;
				
			default:
				System.out.println("Now why did you have to crash the program? why?");
			
			break;
		}
	}
	
	
	public static void looksOutput(){
		System.out.println("[Alright then, tell me, what do you need judged?]");
		Scanner scanner = new Scanner(System.in);
		String looksQuestion = scanner.nextLine();
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("[Looks 5/7.]");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("[A solid 10/10.]");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("[I'm not really sure how to put this into words...]");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("[Absolute shit.]");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("[Great idea, poorly executed.]");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("[In the end, what does my opinion even matter?]");
		else
			System.out.println("[inconclusive.]");
	}
	
	
	public static void moviesAndShowsOutput(){
		System.out.println("[Go ahead then, which movie/show do you want an opinion on?]");
		Scanner scanner = new Scanner(System.in);
		String moviesAndShowsQuestion = scanner.nextLine();
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("[It has an interesting take on the humanity within us all.]");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("[It could have more action, tbh.]");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("[I've always enjoyed good antagonists, but it could have been written better.]");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("[It felt a bit drawn out.]");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("[All things considered, it was pretty good.]");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("[Would be nice with a sequal.]");
		else
			System.out.println("[inconclusive.]");
	}
	
	
	public static void peopleOutput(){
		System.out.println("[Great, who do you want to be judged?]");
		Scanner scanner = new Scanner(System.in);
		String peopleQuestion = scanner.nextLine();
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("[An all around good fellow.]");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("[Funny you should mention that, since that's a really good friend of mine.]");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("[Should burn in hell if you ask me, which you are doing.]");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("[A lazy motherfucker.]");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("[Hate that person, but not just any hate, hate with a passion.]");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("[That person is more like yourself than you know.]");
		else
			System.out.println("[inconclusive.]");
	}
	
	
	/*
	* RNG method which will help determine what the user will be told from the 8Ball.
	*/
	private static double randomInteger(double min, double max){
    double x=(int)(Math.random()*((max-min)+1))+min;
    return x;
	}
}