import java.util.Scanner;

/*
* A magic 8Ball that returns very specific answers, as opposed to "yes", "no", "maybe" etc.
*/
public class WisdomTest{
	public static void main(String[]args){
		// Draws the 8Ball and asks the user what sort of questions will be asked.
		System.out.println("You may ask the magic 8Ball any question you wish");
		System.out.println("   ~         ~         ___           ~           ");
		System.out.println("         ~           /     \\   ~           ~    ");
		System.out.println("     ~          ~   |   8   |     ~    ~         ");
		System.out.println("         ~           \\     /             ~      ");
		System.out.println("    ~        ~         ¨¨¨      ~             ~  ");
		Scanner scanner = new Scanner(System.in);
		System.out.println("First, what sort of question would you like to ask?");
		System.out.println("[1] yes/no/maybe/etc. questions");
		System.out.println("[2] life advice");
		System.out.println("[3] opinions");
		int decision = scanner.nextInt();
		// Switch-statement that will essentially have other switch-statements inside them, by using other methods.
		switch(decision){
			case 1:
				System.out.println("Your question?");
				Scanner scanner1 = new Scanner(System.in);
				String yesNoQuestion = scanner1.nextLine();
				// Answers to question.
				if(randomInteger(0.0, 10.0)<1.0)
					System.out.println("No.");
				else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
					System.out.println("[Yes.]");
				else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
					System.out.println("[Let me think about it.]");
				else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
					System.out.println("[Forget about it.]");
				else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
					System.out.println("[For sure, 100%]");
				else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
					System.out.println("[0%, nope, nah.]");	
				break;
			
			case 2:
				System.out.println("A fortune cookie falls before you, will you open it?");
				Scanner scanner2 = new Scanner(System.in);
				String advice = scanner2.nextLine();
				switch(advice){
					case ("yes"):
						if(randomInteger(0.0,1.0)==0.0)
							System.out.println("Focus on worrying about one thing in life at a time.");
						else if(randomInteger(0.0,1.0)==0.1)
							System.out.println("Advice, when most needed, is least heeded.");
						else if(randomInteger(0.0,1.0)==0.2)
							System.out.println("Everything in moderation, including moderation.");
						else if(randomInteger(0.0,1.0)==0.3)
							System.out.println("Don't count the days, make the days count.");
						else if(randomInteger(0.0,1.0)==0.4)
							System.out.println("It always seems impossible until it's done.");
						else if(randomInteger(0.0,1.0)==0.5)
							System.out.println("Your life is your message, make it a good one.");
						else if(randomInteger(0.0,1.0)==0.6)
							System.out.println("You must be the change you wish to see in the world.");
						else if(randomInteger(0.0,1.0)==0.7)
							System.out.println("Life is actually really simple, we choose to make it complicated.");
						else if(randomInteger(0.0,1.0)==0.8)
							System.out.println("You may not be able to change the world, but a rock in water creates many ripples.");
						else if(randomInteger(0.0,1.0)==0.9)
							System.out.println("Say 'yes' to more things in life, at least more than 'no'.");
						else if(randomInteger(0.0,1.0)==1.0)
							System.out.println("Life carries no meaning, don't look for it, make it.");
						break;
					
					case ("no"):
						System.out.println("Then why the hell did you even choose [2]?");
						break;
				}
				break;
			
			case 3:
				Scanner scanner3 = new Scanner(System.in);
				System.out.println("What would you like my opinion on?");
				System.out.println("[1] Looks");
				System.out.println("[2] Movies/TV Shows");
				System.out.println("[3] People");
				switch(decision){
					case 1:
						Scanner scanner4 = new Scanner(System.in);
						System.out.println("Would you like to be judged?");
						String yesOrNor = scanner4.nextLine();
						switch("yesOrNo"){
							case("yes"):
								looksOutput();
								break;
								
							case("no"):
								System.out.println("I see that your heart is weak, goodbye.");
								break;
						}
						break;
					
					case 2:
						Scanner scanner5 = new Scanner(System.in);
						System.out.println("Would you honestly like my opinion on that?");
						switch("yesOrNo"){
							case("yes"):
								moviesAndShowsOutput();
								break;
								
							case("no"):
								System.out.println("... cya later.");
								break;
						}
						break;
					
					case 3:
						Scanner scanner6 = new Scanner(System.in);
						System.out.println("Would you honestly like my opinion on a person?");
						switch("yesOrNo"){
							case("yes"):
								peopleOutput();
								break;
								
							case("no"):
								System.out.println("... alright, I'm out.");
								break;
						}
						break;
					
					default:
						System.out.println("Please try again later, and use a valid input next time.");
						break;
				}
				break;
			
			default:
				System.out.println("Please enter valid value.");
				break;
		}
	}	
	

	
	public static void looksOutput(){
		System.out.println("Alright then, tell me, what do you need judged?");
		Scanner scanner = new Scanner(System.in);
		String looksQuestion = scanner.nextLine();
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("1");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("2");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("3");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("4");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("5");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("6");
	}
	
	
	public static void moviesAndShowsOutput(){
		System.out.println("Go ahead then, which movie/show do you want an opinion on?");
		Scanner scanner = new Scanner(System.in);
		String moviesAndShowsQuestion = scanner.nextLine();
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("7");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("8");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("9");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("10");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("11");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("12");
	}
	
	
	public static void peopleOutput(){
		System.out.println("Great, who do you want to be judged?");
		Scanner scanner = new Scanner(System.in);
		String peopleQuestion = scanner.nextLine();
		if(randomInteger(0.0, 10.0)<1.0)
			System.out.println("13");
		else if(1.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<2.5)
			System.out.println("14");
		else if(2.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<3.5)
			System.out.println("15");
		else if(3.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<5.0)
			System.out.println("16");
		else if(5.0<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<7.5)
			System.out.println("17");
		else if(7.5<randomInteger(0.0, 10.0) && randomInteger(0.0, 10.0)<10.0)
			System.out.println("18");
	}
	
	
	
	/*
	* RNG method which will help determine what the user will be told from the 8Ball.
	*/
	private static double randomInteger(double min, double max){
    double x=(int)(Math.random()*((max-min)+1))+min;
    return x;
	}
}