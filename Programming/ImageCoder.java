/*
* Displays the image with implemented methods.
* Image.class file contains the neccesary methods to display image.
*/
public class ImageCoder{   
    public static void main(String[] args){
		Image image=new Image("Cringe.jpg");
		addRectangle(image,0,112,500,150,0,100,0);
		addCircle(image,0,300,5500,100,0,100);
		addCircle(image,0,75,5500,100,0,0);
		addCircle(image,500,187,5500,100,0,0);
		image.display();
	}

/*
* Adds a rectangle to the displayed image.
*/
	private static void addRectangle(Image image, int x, int y, int width, int height, int red, int green, int blue){
		int maxX=Math.min(x+width,image.width()-1);
		int maxY=Math.min(y+height,image.height()-1);
		for(int i=x;i<maxX;i++)
			for(int j=y;j<maxY;j++)
				image.setPixel(i,j,red,green,blue);
	}
	
/*
* Adds a circle to the displayed image.
*/
	private static void addCircle(Image image, int x, int y, int radius, int red, int green, int blue){
		int maxX=Math.min(x+radius,image.width()-1);
		int maxY=Math.min(y+radius,image.height()-1);
		int minX=Math.max(x-radius,0);
		int minY=Math.max(y-radius,0);
		for(int i=minX;i<maxX;i++)
			for(int j=minY;j<maxY;j++)
				if(((x-i)*(x-i)+(y-j)*(y-j))<radius)
					image.setPixel(i,j,red,green,blue);
	}
	
/*
* Encrypts the displayed image.
*/
	private static void encrypt(Image image, String key){
		
	}
}