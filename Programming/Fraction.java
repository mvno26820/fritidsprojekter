/*
* A class that works with fractions, used as a subclass for a calculator.
*/ 
public class Fraction{
	/*	
	* I declare the attributes for the methods.
	*/
	private int numerator, denominator;
	
	/*
	* Constructor with zero arguments.
	*/ 
	public Fraction(){
		this(1, 1);			// Calls a constructor.
	}
	
	/*
	* Constructor with one argument.
	*/
	public Fraction(int value){
		this(value, 1);		// Calls a constructor.
	}
	
	/*
	* Constructor with two arguments.
	*/ 
	public Fraction(int num, int den){
		denominator=den;
		numerator=num;
		this(n, d);
		simplify();						// Needs to write the simplify method before this can work, but you know...
	}
	
	/* 
	* Returns the result of adding this fraction with fraction f.
	*/
	public Fraction add(Fraction f){
		return new Fraction(this.numerator*f.denominator+this.denominator*f.numerator, this.denominator*f.denominator);
9	}
	
	/* 
	* Returns the result of subtracting this fraction with fraction f.
	*/ 
	public Fraction subtract(Fraction f){
		return new Fraction(this.numerator*f.denominator-this.denominator*f.numerator, this.denominator*f.denominator);
	}
	
	/*
	* Returns the result of multplying this fraction by fraction f.
	*/
	public Fraction multiply(Fraction f){
		return new Fraction(this.numerator*f.numerator, this.denominator*f.denominator);
	}
	
	/*
	* Returns the results of dividing this fraction by fraction f.
	*/
	public Fraction divide(Fraction f){
		return new Fraction(this.numerator*f.denominator, this.denominator*f.numerator);
	}
	
	/*
	* A method that transforms this fraction into an equivalent irreducible fraction.
	*/ 
	public static void simplify(){
		
	}
	
	
	/*
	*
	*/
	public static void main(String[]args){
		
	}
	
	
	/*
	* Using "instanceof"
	*/
	if(other instanceof Fraction)
	
	
	
	
	
	
	
	
	
	
	
	
}