import java.util.Random;
import java.util.Scanner;
/*
* Takes a user input and returns either win or loss.
*/
public class PingPong{
	public static void main(String[]args){
		int i=0;
		Scanner sc=new Scanner(System.in);
		String name=sc.nextLine();
		while(i<=10){
			if(randomInteger(0.0,10.0)>5){
				System.out.println("Ping");
				System.out.println("Pong");
			}
			else{
				System.out.println("Lost");
				break;
			}
			i=i+1;
		}
	}
	
	/*
	* RNG class which will be used in main method.
	*/
	private static double randomInteger(double min, double max){
    double x=(int)(Math.random()*((max-min)+1))+min;
    return x;
	}
}