public class Weekday {
	
	public static void main (String[]args){
		int day=16, month=9, year=2019;
		int dayCount=day;
		//we add the months in this year
		int i=month-1;
		while (i>0){
			dayCount=dayCount+daysInMonth(i,year);
			i=i-1;
		}
		//we now add the years
		i=year-1;
		while(i>1899){
			dayCount=dayCount+daysInYear(i);
			i=i-1;
		}
		int weekday=dayCount%7;
		String dayName="test";
		if (weekday==0)
			dayName="Sunday";
		else if (weekday==1)
			dayName="Monday";
		else if (weekday==2)
			dayName="Tuesday";
		else if (weekday==3)
			dayName="Wednesday";
		else if (weekday==4)
			dayName="Thursday";
		else if (weekday==5)
			dayName="Friday";
		else if (weekday==6)
			dayName="Saturday";
		System.out.println(day+"/"+month+"/"+year+" was a "+dayName+".");
	}
	
	/*
	* Return the number of days in a month (1<=month<=12)
	*/
	private static int daysInMonth(int month, int year){
		if (month==2 && inLeapYear(year))
			return 29;
		else if (month==2)
			return 28;
		else if (month==4)
			return 30;
		else if (month==6)
			return 30;
		else if (month==9)
			return 30;
		else if (month==11)
			return 30;
		else return 31;
	}
	
	/*
	* Return the numbers of days in a year
	*/
	private static int daysInYear (int year){
		if (inLeapYear(year))
			return 366;
		else return 365;
	}
	
	/*
	* Determines whether a year is a leap year
	*/
	private static boolean inLeapYear (int year){
		return (year%4==0&&(year%100!=0||year%400==0));
	}	
}