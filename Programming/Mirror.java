import java.io.*;
import java.util.*;

/*
* Program that reads input from a file and copies a line by line reversal into another file.
*/
public class Mirror{
	public static void main(String[]args) throws IOException{
		// reads the input file.
		File input = new File("abc.txt");
		FileReader reader = new FileReader(input);
		BufferedReader bufferedReader=new BufferedReader(reader);
		String reversal = bufferedReader.readLine();
		
		// writes output into the file, cba.txt.
		File output = new File("cba.txt");
		FileWriter writer = new FileWriter(output);
		BufferedWriter bufferedWriter = new BufferedWriter(writer);
		while(reversal != null){										// atm, the loop only prints a copy of abc.text into cba.text.
			bufferedWriter.write(reversal);
			bufferedWriter.newLine();
			System.out.println("bippity burst, your lines are reversed.");
			System.out.println("you better pray, the lines are in cba.");
			reversal = bufferedReader.readLine();
		}
		bufferedWriter.close();
	}
}

/*
* Okay, so what I want to do now is make the output individual words reversed in the while-loop
* how i do that however, I'm not entirely sure. I guess the thing the loop has to do is go to the
* first space, then take the characters up until that point and view that as a string, then reverse.
* But how am I to do that?
*/