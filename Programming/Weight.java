/*
* Checks your weight and gives a comment on it.
*/
import java.util.Scanner;
public class Weight{
	public static void main(String[]args){
		int weight;
		System.out.print("Please enter your weight: ");
		Scanner keyboard=new Scanner(System.in);
		weight=keyboard.nextInt();
		if(weight<30){
			System.out.println("Tag i det mindste det her lidt seriøst.");
		}
		else if(weight>29 && weight<40){
			System.out.println("Hvis du ikke er et meget lille barn, er jeg bekymret.");
		}
		else if(weight>49 && weight<55){
			System.out.println("Nu var testen nu givet til en voksen, men whatever.");
		}
		else if(weight>54 && weight<70){
			System.out.println("Med mindre du er et barn... er du en løgner.");
		}
		else if(weight>69 && weight<85){
			System.out.println("Aaaaargh.");
		}
		else if(weight>84 && weight<95){
			System.out.println("Hmmmm, så lad da gå.");
		}
		else if(weight>94 && weight<105){
			System.out.println("Du kan godt se hvad du bliver nødt til at gøre ikke?");
		}
		else if(weight>104 && weight<130){
			System.out.println("Skuffet, bare skuffet.");
		}
		else if(weight>129){
			System.out.println("Tag i det mindste det her lidt seriøst.");
		}
	}
}