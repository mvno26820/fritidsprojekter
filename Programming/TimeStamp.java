/*
* A time management class that will work with hours, minutes and seconds.
*/
public class TimeStamp{
	// First I write the initializers.
	private int hours;
	private int minutes;
	private int seconds;


	/*
	* Checks whether its given arguments can be passed along to the constructor.
	*/
	public static boolean valid(int hours, int minutes, int seconds){
		if(hours<0 || hours>23)
			return false;
		else
			return true;
		
		if(minutes<0 || minutes>59)
			return false;
		else
			return true;
		
		if(seconds<0 || seconds>59)
			return false;
		else
			return true;
	}
	

	// Constructors with 0,1,2 and 3 arguments.
	public TimeStamp(){
		System.out.println("Yeet");
	}
	
	public TimeStamp(int hours){
		this.hours=hours;
	}
	
	public TimeStamp(int hours, int minutes){
		this.hours=hours;
		this.minutes=minutes;
	}
	
	public TimeStamp(int hours, int minutes, int seconds){
		this.hours=hours;
		this.minutes=minutes;
		this.seconds=seconds;
	}

	
	/*
	* Methods for skipping one second, minute and hour.
	*/
	public static void skipSecond(){
		if(this.seconds != 59)
			return new TimeStamp(this.seconds+1);
		else
			return new TimeStamp(this.seconds-this.seconds);
			skipMinute();
	}
	
	public static void skipMinute(){
		if(this.minutes != 59)
			return new TimeStamp(this.minutes+1);
		else
			return new TimeStamp(this.minutes-this.minutes);
			skipHour();
	}
	
	public static void skipHour(){
		if(this.hours != 23)
			return new TimeStamp(this.hours+1);
		else
			return new Timestamp(this.hours-this.hours);
	}
	
	
	/*
	* Method that skips amount of time given, on TimeStamp form.
	*/
	public static void skip(TimeStamp time){
		if(this.hours+time.hours<24 && this.minutes+time.minutes<60 && this.seconds+time.seconds<60)
			return new TimeStamp(this.hours+time.hours, this.minutes+time.minutes, this.seconds+time.seconds);
		
		else if(this.hours+time.hours<24 && this.minutes+time.minutes<60 && this.seconds+time.seconds>60)
			return new TimeStamp(this.hours+time.hours, this.minutes+time.minutes+1, this.seconds+time.seconds-60);
		
		else if(this.hours+time.hours<24 && this.minutes+time.minutes>60 && this.seconds+time.seconds>60)
			return new TimeStamp(this.hours+time.hours+1, this.minutes+time.minutes+1-60, this.seconds+time.seconds-60);
		
		else if(this.hours+time.hours<24 && this.minutes+time.minutes>60 && this.seconds+time.seconds<60)
			return new TimeStamp(this.hours+time.hours+1, this.minutes+time.minutes-60, this.seconds+time.seconds);
		
		else if(this.hours+time.hours>24 && this.minutes+time.minutes>60 && this.seconds+time.seconds>60)
			return new TimeStamp(this.hours+time.hours+1-24, this.minutes+time.minutes+1-60, this.seconds+time.seconds-60);
		
		else if(this.hours+time.hours>24 && this.minutes+time.minutes<60 && this.seconds+time.seconds>60)
			return new TimeStamp(this.hours+time.hours-24, this.minutes+time.minutes+1, this.seconds+time.seconds-60);
		
		else if(this.hours+time.hours>24 && this.minutes+time.minutes>60 && this.seconds+time.seconds<60)
			return new TimeStamp(this.hours+time.hours+1-24, this.minutes+time.minutes-60, this.seconds+time.seconds);
	}
	
	
	/*
	* Method to determine whether this TimeStamp is the same as other.
	*/ 
	public static boolean equals(Object other){
		if(this.hours==other.hours && this.minutes=other.minutes && this.seconds==other.seconds)
			return true;
		else
			return false;
	}
	
	
	/*
	* Method that returns af copy of TimeStamp.
	*/ 
	public static TimeStamp copy(){
		TimeStamp object2 = object1;
		return object2;
	}
	
	
	/*
	* Method that returns a textual representation of this TimeStamp.
	*/
	public static String toString(){
		return hours+":"+minutes+":"+seconds;
	}
	
	
	public static void main(String[]args){
		TimeStamp object1 = new TimeStamp(20,45,7);
		System.out.println(object1);
	}	
}