public class Roman{
	public static void main(String[]args){
		numToRoman(2018);
	}

	/*
	* Translates an integer number into a Roman numerals.
	*/
	private static String numToRoman(int n){
		int I=1,V=5,X=10,L=50,C=100,D=500,M=1000;
		while(n>0){
			if(n>=1000){
			System.out.print("M");
			n=n-1000;
			}
			else if((n>=500)&&(n<1000)){
			System.out.print("D");
			n=n-500;
			}
			else if((n>=100)&&(n<500)){
			System.out.print("C");
			n=n-100;
			}
			else if((n>=50)&&(n<100)){
			System.out.print("L");
			n=n-50;
			}
			else if((n>=10)&&(n<50)){
			System.out.print("X");
			n=n-10;
			}
			else if((n>=5)&&(n<10)){
			System.out.print("V");
			n=n-5;
			}
			else if((n>=1)&&(n<5)){
			System.out.print("I");
			n=n-1;
			}
			else
				System.out.print("Invalid");
		}
	}
}