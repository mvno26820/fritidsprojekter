/*
* Denne class skal definere hvert et korts suit type fra
* suit class, value i form af int, og navn i form af String.
* Derudover skal den returnere værdien for point alt efter
* hvor meget kortet er værd, isHigherThan til at dømme om
* this card har større værdi end card og returnere en
* tekstuel repræsentation af kortet med en toString.
*/
public enum Card{
	/*
	* Først opskriver vi alle 40 forskellige typer af kortet
	* der kan fremkomme, med deres værdi, suit og navn.
	* Vi lader værdierne for kort der ikke giver point være
	* negative, så at de bliver sorteret fra i points().
	* Først er alle kort fra suit Spades:
	*/
	ACE_SPADES(Suit.SPADES, 11, "Ace"),
	TWO_SPADES(Suit.SPADES, -5, "Two"),
	THREE_SPADES(Suit.SPADES, -4, "Three"),
	FOUR_SPADES(Suit.SPADES, -3, "Four"),
	FIVE_SPADES(Suit.SPADES, -2, "Five"),
	SIX_SPADES(Suit.SPADES, -1, "Six"),
	SEVEN_SPADES(Suit.SPADES, 10, "Seven"),
	JACK_SPADES(Suit.SPADES, 2, "Jack"),
	QUEEN_SPADES(Suit.SPADES, 3, "Queen"),
	KING_SPADES(Suit.SPADES, 4, "King"),
	
	/*
	* Herunder af alle kort fra suit Clubs.
	*/
	ACE_CLUBS(Suit.CLUBS, 11, "Ace"),
	TWO_CLUBS(Suit.CLUBS, -5, "Two"),
	THREE_CLUBS(Suit.CLUBS, -4, "Three"),
	FOUR_CLUBS(Suit.CLUBS, -3, "Four"),
	FIVE_CLUBS(Suit.CLUBS, -2, "Five"),
	SIX_CLUBS(Suit.CLUBS, -1, "Six"),
	SEVEN_CLUBS(Suit.CLUBS, 10, "Seven"),
	JACK_CLUBS(Suit.CLUBS, 2, "Jack"),
	QUEEN_CLUBS(Suit.CLUBS, 3, "Queen"),
	KING_CLUBS(Suit.CLUBS, 4, "King"),
	
	/*
	* Herunder er alle kort fra suit Hearts.
	*/
	ACE_HEARTS(Suit.HEARTS, 11, "Ace"),
	TWO_HEARTS(Suit.HEARTS, -5, "Two"),
	THREE_HEARTS(Suit.HEARTS, -4, "Three"),
	FOUR_HEARTS(Suit.HEARTS, -3, "Four"),
	FIVE_HEARTS(Suit.HEARTS, -2, "Five"),
	SIX_HEARTS(Suit.HEARTS, -1, "Six"),
	SEVEN_HEARTS(Suit.HEARTS, 10, "Seven"),
	JACK_HEARTS(Suit.HEARTS, 2, "Jack"),
	QUEEN_HEARTS(Suit.HEARTS, 3, "Queen"),
	KING_HEARTS(Suit.HEARTS, 4, "King"),
		
	/*
	* Herunder er alle kort fra suit Diamonds.
	*/
	ACE_DIAMONDS(Suit.DIAMONDS, 11, "Ace"),
	TWO_DIAMONDS(Suit.DIAMONDS, -5, "Two"),
	THREE_DIAMONDS(Suit.DIAMONDS, -4, "Three"),
	FOUR_DIAMONDS(Suit.DIAMONDS, -3, "Four"),
	FIVE_DIAMONDS(Suit.DIAMONDS, -2, "Five"),
	SIX_DIAMONDS(Suit.DIAMONDS, -1, "Six"),
	SEVEN_DIAMONDS(Suit.DIAMONDS, 10, "Seven"),
	JACK_DIAMONDS(Suit.DIAMONDS, 2, "Jack"),
	QUEEN_DIAMONDS(Suit.DIAMONDS, 3, "Queen"),
	KING_DIAMONDS(Suit.DIAMONDS, 4, "King");
	
	/*
	* Her definerer vi således attributterne som vi i
	* følgende metoder vil bruge til constructor og 
	* videre i de tre underliggende metoder: points(),
	* isHigherThan() og toString(). Vi gør Suit suit
	* public idet at det også skal bruges uden for denne
	* class.
	*/
	public final Suit suit;
	private final int value;
	private final String name;
	
	/*
	* Først bruger vi constructor med tre argumenter til
	* at initialisere suit, value og name.
	*/
	private Card(Suit suit, int value, String name){
		this.suit = suit;
		this.value = value;
		this.name = name;
	}
	
	/*
	* Her bruger vi så this value i et boolean statement
	* som tjekker hvorvidt om this value er mindre end 0
	* og hvis det opfyldes, bliver 0 returneret. Hvis det
	* ikke opfyldes, vil this value bliver returneret.
	*/
	public int points(){
		return (this.value < 0) ? 0 : this.value;
	}
	
	/*
	* Returnere true hvis this card har større value end
	* card value, ellers false. 
	*/
	public boolean isHigherThan(final Card card){
		return this.value > card.value;
	}
	
	/*
	* Denne metodes funktion er at returnere this name som
	* kommer fra navnet på selve kortet, f.eks. "Six" i String 
	* samt en tekstuel repræsentation for this suit.
	*/
	public String toString(){
		return this.name + " of " + this.suit.toString();
	}	
}