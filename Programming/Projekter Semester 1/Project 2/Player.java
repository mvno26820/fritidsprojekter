import java.util.ArrayList;
import java.util.List;

public class Player{
	
	private String name;
	private Card[] hand;
	private List<Card> collectedCards;
	
	/*
	* Laver en ny spiller med et givent navn, uden nogle kort i hånden 
	eller i bunken af samlede kort.
	*/
	public Player(String name) {
		this.name = name;
		this.hand = new Card[0];
		int i = 0;
		this.hand[i] = null;
		this.collectedCards = new ArrayList<Card>();
		
	}
	
	/*
	* Returnere spillerens navn
	*/
	public String name() {
		return this.name;
	}
	
	/*
	* Returnere kortene i spillerens hånd.
	*/
	public Card[] hand() {
		final Card[] array = new Card[this.cardsInHand()];
		int n = 0;
		for (int i = 0; i < 3; i++) {
			if (this.hand[i] !=null) {
				array[n] = this.hand[i];
				n = n + 1;;
			}
		}
		return array;
	}
	
	/*
	* Returnere antalet af kort i spillerens hånd.
	*/
	public int cardsInHand() {
		int n = 0;
		for (int i = 0; i < 3; i++) {
			if (this.hand[i] != null) {
				n = n + 1;;
			}
		}
		return n;
	}
	
	/*
	* Tilføjer et kort til spillerens hånd. (der skal være mindre end 3 kort i spillerenes hånd 
	før der kan tilgøjes flere)
	*/
	public void addToHand(final Card card) {
		int n = 0;
		int n2 = 0;
		while (n < 3 && n2 == 0) {
			if (this.hand[n] == null) {
				this.hand[n] = card;
				n2 = 1;
			}
			else {
				n = n + 1;
			}
		}
	}
	
	/*
	* Fjerner et kort fra spillerens hånd. (Kortet skal være i hånden for at det kan fjernes)
	*/
	public void removeFromHand(final Card card) {
		for (int i = 0; i < 3; i++) {
			if (this.hand[i] == card) {
				this.hand[i] = null;
			}
		}
	}
	
	/*
	* Returnere et array af de kort der er indsamlet af spilleren.
	*/
	public Card[] collectedCards() {
		return this.collectedCards.toArray(this.hand);
	}
	
	/*
	* Tilføjer et kort til bunken af kort indsamlet af spilleren.
	*/
	public void addToCollectedCards(final Card card) {
		this.collectedCards.add(card);
	}
}