/* 
* denne class skal bruges til at definere de 4 suits i kortspillet,
* som skal bruges i Card.class for at kunne definere hvert enkelt kort,
* der findes i vores kortspil på 40 kort.
*/
public enum Suit{
	HEARTS,
	SPADES,
	DIAMONDS,
	CLUBS;
}