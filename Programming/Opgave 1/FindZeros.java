/*
* Find the least value for n that allows f(n)=0
*/
public class FindZeros{
	public static void main(String[]args){
		int n=-75;
		System.out.println("Ved input af "+n+" opskrives trinnene frem til 0:");
		while(f(n)!=0){
			n=n+1;
		}
		System.out.println(n);
	}
	
	private static int f(int n){
		return n+n-5;
	}
}