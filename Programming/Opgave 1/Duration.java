public class Duration{
	/*
	* convert a time interval seconds into hours, minutes and seconds
	*/
	public static void main (String[] args){
		int inputInSeconds=7887;
		int outputInSeconds=inputInSeconds%60;
		int outputInMinutes=(inputInSeconds/60)%60;
		int outputInHours=(inputInSeconds/60)/60;
		System.out.println(inputInSeconds+" corresponds with "+outputInHours
		 +" hours, "+outputInMinutes+" minutes and "+outputInSeconds+".");
	}
}