public class Bisection{
	public static void main(String[]args){
		double a=5.0,b=6.0,c,error=0.000000001;
		if(f(a)*f(b)>0)
			System.exit(1);
		while(b-a>error){
			c=(a+b)/2;
			if((f(a)*f(c))<0)
				b=c;
			else
				a=c;
		}
		System.out.println("The solution to f(x)=0 is "+a+".");
	}
	
	public static double f(double x){
		return x*x-2;
	}
}