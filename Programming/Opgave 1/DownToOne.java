public class DownToOne{
	public static void main (String[]args){
		downToOne(254);
	}
	
	public static void downToOne(int n){
		int i=0;
		while(n>1){
			if(n%2==0){
				n=n/2;
				System.out.println("n bliver divideret med 2 og bliver til: "+n);
			}
			else{
				n=n*3+1;
				System.out.println("n bliver ganget med 3 og adderet med 1: "+n);
			}
			i=i+1;
		}
		System.out.println("Antallet er trin der tages ved n="+254+" er: "+i);
	}
}