public class Time{
	/*
	* convert a time interval given in hours, minutes and seconds into seconds
	*/
	public static void main (String[] args){
		int inputInSeconds=0;
		int inputInMinutes=40;
		int inputInHours=1;
		int outputInSeconds=(inputInHours*3600)+(inputInMinutes*60)+inputInSeconds;
		
		System.out.println(inputInHours+" hours, "+inputInMinutes+" minutes and "+inputInSeconds+" seconds "+"is in total = "+outputInSeconds+"seconds.");
	}
}