
public class Stars{
/*
* Print n stars.
* Precondition: n>=0
*/
	public static void stars (int n){
			printChars('*',n);
		}
		
/*
* Print n stars.
* Precondition: n>=0
*/		
	public static void linesOfStars(int n){
		int i=1;
		while(i<=n){
			stars(i);
			i=i+1;
		}
	}
	
	public static void triangle(int n){
		int i=1;
	}
	
	
	private void printChars(char c, int n){
		int i=0;
		while(i<n){
			System.out.print(c);	
			i=i+1;
		}
		System.out.println( );
	}
	
	public static void main(String[]args){
		stars(5);
		linesOfStars(5);
		triangle(5);
	}
}