/*
* A program for calculating factorials, also to illustrate how
* to fix the error of "non-static to static".
*/
public class Factorial{
	public static void main(String[]args){
		Factorial instance = new Factorial();
		System.out.println(instance.factorial(12));
	}
	
	public int factorial(int n){
		if(n==1){
			return 1;
		}
		return n*factorial(n-1);
	}
}