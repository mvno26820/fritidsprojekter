import java.util.Scanner;

// Java implementation of the approach 
public class PalindromeChecker{ 
	static boolean isPalindrome(String input){ 
		int i = 0, j = input.length() - 1; 
		while (i < j){ 
			if (input.charAt(i) != input.charAt(j)) 
				return false; 
			i++; 
			j--; 
		} 
		return true; 
	} 

	public static void main(String[] args){
		System.out.println("Please type in your word of inquire, keep in mind ");
		System.out.println("that capital letters are seen as different.");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		if (isPalindrome(input)) 
			System.out.print("Yes, '"+input+"' is a palindrome."); 
		else
			System.out.print("No, '"+input+"' is not a palindrome."); 
	} 
} 