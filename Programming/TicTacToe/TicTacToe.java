/**
 * Second implementation: the board is a bi-dimensional array of char.
 * There is an attribute with the number of moves played so far -- this gives
 * a more efficient implementation of isFull() and removes the need for an
 * explicit attribute storing the next player.
 *
 * Cell i is in row (i-1)/3, column (i-1)%3.
 */
public class TicTacToe {

    private char[][] board;
    private int moves;
    private static final char EMPTY = '-';

    /**
     * Creates an empty board where the next player is 'X'.
     */
    public TicTacToe() {
        board = new char[3][3];
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
            board[i][j] = EMPTY;
        moves = 0;
    }

    /**
     * Checks whether a given cell is free.<br>
     * <b>Precondition:</b> 1&le;cell&le;9.
     * @param cell the number of the cell
     * @return true if the cell is currently unoccupied
     */
    public boolean isFree(int cell) {
        return (board[(cell-1)/3][(cell-1)%3] == EMPTY);
    }

    /**
     * Returns the char for the player who has played in the given cell.<br>
     * <b>Precondition:</b> 1&le;cell&le;9 &amp;&amp; !isFree(cell)
     * @param cell the number of the cell
     * @return the char for the player who has played in cell
     */
    public char value(int cell) {
        return board[(cell-1)/3][(cell-1)%3];
    }

    /**
     * Returns the char of the player whose turn it is.
     * @return the char of the next player
     */
    public char nextPlayer() {
        return ((moves%2 == 0) ? 'N' : 'O');
    }

    /**
     * Checks whether all cells are occupied.
     * @return true if the board has no empty cells
     */
    public boolean isFull() {
        return (moves >= 9);
    }

    /**
     * Plays the next move in the given cell.<br>
     * <b>Precondition:</b> 1&le;cell&le;9 &amp;&amp; isFree(cell)
     * @param cell the cell to play in
     */
    public void play(int cell) {
        board[(cell-1)/3][(cell-1)%3] = nextPlayer();
        moves = moves + 1;
    }

    /**
     * Checks whether the player with a given char has won.<br>
     * <b>Precondition:</b> player == 'O' || player == 'X'
     * @param player the char corresponding to a player
     * @return true if player has won
     */
    public boolean hasWon(char player) {
        boolean hasWon = false;
        // rows and columns
        for (int i=0; i<3; i++)
            if ((board[i][0] == player) && (board[i][1] == player) && (board[i][2] == player))
                hasWon = true;
            else if ((board[0][i] == player) && (board[1][i] == player) && (board[2][i] == player))
                hasWon = true;
        if ((board[0][0] == player) && (board[1][1] == player) && (board[2][2] == player))
            hasWon = true;
        else if ((board[0][2] == player) && (board[1][1] == player) && (board[2][0] == player))
            hasWon = true;
        return hasWon;
    }

    /*
     * Auxiliary method for printing a cell.
     */
    private String cellToString(int row, int col) {
        if (board[row][col] == EMPTY)
            return " "+(row*3+col+1)+" ";
        else
            return " "+board[row][col]+" ";
    }
    
    /**
     * Returns a textual representation of this board.
     * @return a textual representation of this board
     */
    public String toString() {
        String result = "";
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                result = result + cellToString(i,j);
                if (j<2)
                    result = result + "|";
                else
                    result = result + "\n";
            }
            if (i<2)
                result = result + "---+---+---\n";
        }
        return result;
    }
    
}