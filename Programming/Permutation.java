import java.util.*;

// Java program to print all permutations of a given string. 
public class Permutation{
	public static void main(String[] args) throws InterruptedException{
		System.out.println("Input a string, which will be permuted.");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		
		long start = System.currentTimeMillis();
		System.out.println("Below is all possible permutations of ["+input+"]");
		int n = input.length(); 
		Permutation permutation = new Permutation(); 
		permutation.permute(input, 0, n-1);
		System.out.println("The total amount of permutations from ["+input+"] is: "+counter);
		long end = System.currentTimeMillis();
		long elapsed = end - start;
		
		System.out.println("Execution time: "+elapsed+"ms");
	}
	
	private static int counter = 0;
	
	/*
	* permutation function 
	* @param input string to calculate permutation for 
	* @param l starting index 
	* @param r end index 
	*/
	private void permute(String input, int l, int r){
		if (l == r){
			System.out.println(input);
			counter++;
		}
		else{ 
			for (int i = l; i <= r; i++){ 
				input = swap(input,l,i); 
				permute(input, l+1, r); 
				input = swap(input,l,i);
			} 
		}
	}

	/* 
	* Swap Characters at position 
	* @param a string value 
	* @param i position 1 
	* @param j position 2 
	* @return swapped string 
	*/
	public String swap(String a, int i, int j) { 
		char temp; 
		char[] charArray = a.toCharArray(); 
		temp = charArray[i] ; 
		charArray[i] = charArray[j];
		charArray[j] = temp; 
		return String.valueOf(charArray); 
	} 
}