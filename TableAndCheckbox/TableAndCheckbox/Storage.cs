﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace TableAndCheckbox
{
    public class Storage
    {
        public int Row { get; set; }
        public int Level { get; set; }
        public bool Include { get; set; }
    }
}
