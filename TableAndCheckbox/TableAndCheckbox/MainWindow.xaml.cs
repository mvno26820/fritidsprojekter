﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TableAndCheckbox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Storage> users = new List<Storage>();
            users.Add(new Storage() { Row = 14, Level = 25, Include = false });
            users.Add(new Storage() { Row = 24, Level = 35, Include = false });
            users.Add(new Storage() { Row = 34, Level = 45, Include = false });
            users.Add(new Storage() { Row = 34, Level = 45, Include = false });

            DataGridSimple.ItemsSource = users;
            
        }

        /*
        private void DataGridSimple_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Storage.Row):
                    e.Column.Header = "Name";
                    break;

                case nameof(Storage.Level):
                    e.Column.Header = "Claim Number";
                    break;
            }
        }
        */
    }
}
