﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniOpgaver
{
    class Permutation
    {
        public void Permute(string input, int n)
        {
            // If there are no other permutations that the string itself, for example: "a".
            if (n == (input.Length - 1))
                Console.WriteLine(input);
            // If there are permutations, then runs the swapping method the same amount of times that there are chars in the array.
            else
            {
                for (int i = n; i <= input.Length - 1; i++)
                {
                    input = Swap(input, n, i);
                    Permute(input, n + 1);
                    input = Swap(input, n, i);
                }
            }
        }

        // The swapping method.
        public string Swap(string swapString, int i, int j)
        {
            // Makes a temporary variable of type char.
            char tempChar;
            // Makes an array of char datatypes and uses the method ToCharArray on the string swapString.
            char[] charArray = swapString.ToCharArray();

            // Swapper-ooohh.
            tempChar = charArray[i];
            charArray[i] = charArray[j];
            charArray[j] = tempChar;

            // New string that's swapped.
            string s = new string(charArray);

            // Returns swapped string.
            return s;
        }
    }
}