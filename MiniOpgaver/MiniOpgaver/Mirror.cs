﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace MiniOpgaver
{
	class Mirror
	{
		public void Mirroring()
		{
			// Incase something happens to the files, I made it a try-catch-finally.
            try
            {
				// Makes a variable for both paths to the .txt files.
				string pathABC = @"C:\Users\mikkel\OneDrive\Skrivebord\fritidsprojekter\MiniOpgaver\MiniOpgaver\ABC.txt";
				string pathCBA = @"C:\Users\mikkel\OneDrive\Skrivebord\fritidsprojekter\MiniOpgaver\MiniOpgaver\CBA.txt";

				// Reads all the text withing ABC.txt.
				string str = File.ReadAllText(pathABC);

				// Makes an empty string and fills it from the last char in the .txt file and moving left.
				string reverse = null;
				for (int i = str.Length - 1; i > -1; i--)
				{
					reverse += str[i];
				}

				// Clears the output .txt file and write in the reverses (mirrored) .txt file.
				File.WriteAllText(pathCBA, null);
				File.AppendAllText(pathCBA, reverse);
			}
			catch(FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("bibbipty burst, your text is reversed");
            }
			
		}
	}
}