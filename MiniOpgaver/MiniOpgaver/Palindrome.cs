﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniOpgaver
{
    class Palindrome
    {
		public bool isPalindrome(string input)
		{
			// Makes 2 int-types that corresponds with each end of the string.
			int i = 0;
			int j = input.Length - 1;

			// Runs the while-loop as long as you haven't reached the middle of the string yet.
			while (i < j)
			{
				// As soon as the if-statement figures out that something isn't a palindrome, it returns false.
				if (input[i] != input[j])
					return false;

				// Both i and j moves closer to the middle, the point where the while-loop will stop.
				i++;
				j--;
			}

			// If the while-loop fully runs through, it returns true because there hasn't been found any reason to make it "not a palindrome".
			return true;
		}
	}
}
