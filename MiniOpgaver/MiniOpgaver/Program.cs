﻿using System;

namespace MiniOpgaver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to mini-tasks. \nPlease choose what you would like to do?");
            Console.WriteLine("1. Permutations \n2. Palindrome \n3. Mirror \n4. 'What day?'");

            int menu = Convert.ToInt32(Console.ReadLine());
            switch (menu)
            {
                case 1:
                    #region Permutation
                    Console.WriteLine("Input string to permute:");
                    string input = Console.ReadLine();
                    Console.WriteLine("\n");

                    // Creates new instance of Permutation.
                    Permutation permutation = new Permutation();
                    permutation.Permute(input, 0);
                    #endregion
                    break;

                case 2:
                    #region Palindrome
                    Console.WriteLine("Input string to palindrome-check:");
                    string input2 = Console.ReadLine();
                    Console.WriteLine("\n");

                    // Checks if input string is a palindrome.
                    Console.WriteLine("\nIs the string a palindrome?");
                    // Creates new instance of palindrome.
                    Palindrome palindrome = new Palindrome();
                    // Outputs results.
                    if (palindrome.isPalindrome(input2))
                        Console.WriteLine($"Yes, '{input2}' is a palindrome.");
                    else
                        Console.WriteLine($"No, '{input2}' is NOT a palindrome.");
                    #endregion
                    break;

                case 3:
                    #region Mirror
                    Mirror mirror = new Mirror();
                    mirror.Mirroring();
                    #endregion
                    break;

                case 4:
                    #region What Day?
                    Console.WriteLine("\nInput day of month.");
                    int day = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Input month.");
                    int month = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Input year.");
                    int year = Convert.ToInt32(Console.ReadLine());

                    Weekday weekday = new Weekday();
                    weekday.WhatDayIsIt(day, month, year);
                    #endregion
                    break;

                default:
                    Console.WriteLine("Exiting Program...");
                    break;
            }

            // Exits program.
            Console.ReadKey();
        }
    }
}