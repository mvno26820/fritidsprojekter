﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniOpgaver
{
	class Weekday
	{
		public void WhatDayIsIt(int day, int month, int year)
		{
			// The amount of days is equal to the day of the month, obviously.
			int dayCount = day;

			// Adds the months already passed in the given year.
			int i = month - 1;
			while (i > 0)
			{
				dayCount = dayCount + daysInMonth(i, year);
				i = i - 1;
			}

			// Adds the years passed. It counts from 1899 because that is the 'zero date', because of
			// how leap years work there was confusion on 1899-12-30 (read on google if curious), but
			// essentially that is the default date for counting start.
			i = year - 1;
			while (i > 1899)
			{
				dayCount = dayCount + daysInYear(i);
				i = i - 1;
			}
			// After the above code has run, the years, months and days have all been 'subtracted'.

			// Uses modulo to find the remainder after dividing with 7, thereby finding a value to use 
			// for finding the name of the day.
			int weekday = dayCount % 7;
			string dayName = "";

            switch (weekday)
            {
				case 0:
					dayName = "Sunday";
					break;
				case 1:
					dayName = "Monday";
					break;
				case 2:
					dayName = "Tuesday";
					break;
				case 3:
					dayName = "Wednesday";
					break;
				case 4:
					dayName = "Thursday";
					break;
				case 5:
					dayName = "Friday";
					break;
				case 6:
					dayName = "Saturday";
					break;
			}

			// Finally prints out the 
            Console.WriteLine($"\n{day}/{month}/{year} was a {dayName}.");
		}

		//Return the number of days in a month (1<=month<=12)
		private static int daysInMonth(int month, int year)
		{
			int returnValue = 0;
            switch (month)
            {
				case 2:
                    if (inLeapYear(year))
                    {
						returnValue = 29;
                    }
                    else if(!inLeapYear(year))
                    {
						returnValue = 28;
                    }
					break;
				case 4:
					returnValue = 30;
					break;
				case 6:
					returnValue = 30;
					break;
				case 9:
					returnValue = 30;
					break;
				case 11:
					returnValue = 30;
					break;
				default:
					returnValue = 31;
					break;
			}

			return returnValue;
		}
		
		//Return the numbers of days in a year
		private static int daysInYear(int year)
		{
			if (inLeapYear(year))
				return 366;
			else 
				return 365;
		}

		//Determines whether a year is a leap year
		private static bool inLeapYear(int year)
		{
			return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		}
	}
}